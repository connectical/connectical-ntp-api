# Connectical NTP API

Status REST API for NTP servers.

## Preliminaries

First of all you should have an NTP server running. Here is how to configure
[chrony](https://chrony.tuxfamily.org/). You can install it from its
package, for example in Arch Linux.
```shell
pacman -S chrony
```

Once installed, modify the configuration and add the following lines in
`/etc/chrony.conf`.
```conf
# In NTP servers section
server hora.roa.es minpoll 8 iburst

pool 0.es.pool.ntp.org iburst
pool arch.pool.ntp.org iburst

# In allow section allow all to act as worldwide server
allow all

local stratum 10
```

And finally start and enable chrony.
```shell
systemctl start chrony
systemctl enable chrony
```

## Installation

Clone the repository and install the code in a virtualenv.
```shell
git clone https://gitlab.com/connectical/connectical-ntp-api.git
cd connectical-ntp-api
virtualenv /opt/connectical-ntp-api-venv
source /opt/connectical-ntp-api-venv/bin/activate
pip install .
```

## Run

Once installed, to run it.
```shell
/opt/connectical-ntp-api-venv/bin/cna server
```

By default it listens to `localhost` on port `9009` and connects to the NTP
server on `127.0.0.1`. You can specify all these values. Run the binary with
no arguments to see the help.

To run it with `systemd`.
```systemd
cat > /etc/systemd/system/connectical-ntp-api.service << EOF
[Unit]
Description=Connectical Time API
Requires=network.target
After=network.target

[Service]
Environment=NTP_API_HOST=127.0.0.1
Environment=NTP_API_PORT=9009
Environment=NTP_SERVER=127.0.0.1
DynamicUser=yes
ExecStart=/opt/connectical-ntp-api-venv/bin/cna server
Restart=always
EOF
```

Start and enable.
```shell
systemctl daemon-reload
systemctl start connectical-ntp-api
systemctl enable connectical-ntp-api
```
