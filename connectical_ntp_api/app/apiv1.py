#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2014-2023 Andrés J. Díaz <ajdiaz@connectical.com>
#                       Óscar García Amor <ogarcia@connectical.com>
#
# Distributed under terms of the GNU GPLv3 license.

from time import gmtime, strftime

import os

import bottle
import ntplib
import calendar

NTP_SERVER=os.environ.get('NTP_SERVER', '127.0.0.1')
TIME_FORMAT='%Y-%m-%dT%H:%M:%SZ'

def get_data():
    try:
        c = ntplib.NTPClient()
        response = c.request(NTP_SERVER, version=3)
        return response
    except:
        bottle.abort(500,'Time server not reachable')

@bottle.get('/api/1/offset')
def offset():
    response = get_data()
    if 'raw' in bottle.request.query.keys():
        bottle.response.content_type = 'text/plain'
        return f'{response.offset}'
    return {"format": "json", "offset": response.offset}

@bottle.get('/api/1/time')
def offset():
    response = get_data()
    time = strftime(TIME_FORMAT, gmtime(response.tx_time))
    if 'raw' in bottle.request.query.keys():
        bottle.response.content_type = 'text/plain'
        return '%s' % time
    return {"time": time,
            "timestamp": response.tx_time}

@bottle.get('/api/1/stats')
def stats():
    response = get_data()
    return {
            "time": strftime(TIME_FORMAT, gmtime(response.tx_time)),
            "offset": response.offset,
            "leap": ntplib.leap_to_text(response.leap),
            "delay": response.root_delay,
            "ref": ntplib.ref_id_to_text(response.ref_id),
            "stratum": ntplib.stratum_to_text(response.stratum),
            "timestamp": response.tx_time
    }
