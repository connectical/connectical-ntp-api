# vim:fenc=utf-8
#
# Copyright © 2014-2023 Andrés J. Díaz <ajdiaz@connectical.com>
#                       Óscar García Amor <ogarcia@connectical.com>
#
# Distributed under terms of the GNU GPLv3 license.

from connectical_ntp_api import app

import os
import sys

DEFAULT_DEVEL_HOST = '127.0.0.1'
DEFAULT_DEVEL_PORT = 9099
DEFAULT_HOST = '127.0.0.1'
DEFAULT_PORT = 9009

def dev():
    """dev [host] [port]
    Run a development server in specific host and port.
    Aditionally you can specify the following environment variables.
    - NTP_SERVER, address of NTP server (default: 127.0.0.1).
    - NTP_API_HOST, the hostname to listen on (default: 127.0.0.1).
    - NTP_API_PORT, the port of the webserver (default: 9099).
    - NTP_API_ENV, the name of environment (default: development).
    Never use this server for production.
    """
    host = sys.argv[2] if len(sys.argv) > 2 else os.environ.get('NTP_API_HOST', DEFAULT_DEVEL_HOST)
    port = int(sys.argv[3]) if len(sys.argv) > 3 else int(os.environ.get('NTP_API_PORT', DEFAULT_DEVEL_PORT))
    wsgi(os.environ.get('NTP_API_ENV', 'development')).run(host=host, port=port)

def wsgi(app_environ='production'):
    """Return WSGI handler for application."""
    return app.AppAPI([app_environ])

def server():
    """server [host] [port]
    Run a production server in specific host and port.
    Aditionally you can specify the following environment variables.
    - NTP_SERVER, address of NTP server (default: 127.0.0.1).
    - NTP_API_HOST, the hostname to listen on (default: 127.0.0.1).
    - NTP_API_PORT, the port of the webserver (default: 9009).
    - NTP_API_ENV, the name of environment (default: production).
    """
    host = sys.argv[2] if len(sys.argv) > 2 else os.environ.get('NTP_API_HOST', DEFAULT_DEVEL_HOST)
    port = int(sys.argv[3]) if len(sys.argv) > 3 else int(os.environ.get('NTP_API_PORT', DEFAULT_DEVEL_PORT))
    from gevent.pywsgi import WSGIServer
    print(f'Serving on {host}:{port}...')
    WSGIServer((host, port), wsgi(os.environ.get('NTP_API_ENV', 'production'))).serve_forever()

def cli():
    """Command line script."""
    if len(sys.argv) > 1:
        if sys.argv[1] == 'cli':
            print('Not allowed')
        else:
            try:
                return globals()[sys.argv[1]]()
            except KeyError:
                print(f'{sys.argv[1]} is not a valid argument')
    else:
        for x, y in globals().items():
            if x != 'cli' and x != 'wsgi' and x[0] != '_' and hasattr(y, '__call__') and hasattr(y, '__doc__'):
                print('%s' % (y.__doc__,))
