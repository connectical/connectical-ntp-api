#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2014-2023 Andrés J. Díaz <ajdiaz@connectical.com>
#                       Óscar García Amor <ogarcia@connectical.com>
#
# Distributed under terms of the GNU GPLv3 license.

from connectical_ntp_api.main import cli

if __name__ == "__main__":
    cli()
